/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {Subscription, Subscriber, Observable, Stream } from "most";
import symbolObservable from "symbol-observable";

export type Emitter<A> = (a: Array<Subscriber<A>>) => void;

interface SubSub<A> {
    subscriber: Subscriber<A>,
    subscription: Subscription<A>
}


const createSubscriber = 
    function<A>(observer: Subscriber<A>): SubSub<A> {
        let alive = true;
        const s:Subscriber<A> = {
            next: (v: A) => {
                if (alive) observer.next(v);
            },
            error: (err: Error) => {
                if (alive) observer.error(err);
            },
            complete: () => {
                if (alive) observer.complete();
            },
        };
        return {
            subscriber: s,
            subscription: {
                unsubscribe: () => {
                    alive = false;
                }
            }
        };
    };


const createSubscribe = 
    function<A>(subs: Array<Subscriber<A>>): ((observer: Subscriber<A>) => void) {
        return (observer) => {
            const ss = createSubscriber<A>(observer);
            subs.push(ss.subscriber);
            return ss.subscription;
        };
    };

export const create =
    function<A>(emitter: Emitter<A>): ((b: Array<Subscriber<A>>) =>  any) {
        return (subs) => {
            // emitter takes over the list of suscribers
            // and we avoid attaching it to a specific instance, the caller being
            // in charge of managing it;
            emitter(subs);

            const observable: () => any = //WARNING: Observable<A> = // but ts does not know Symbol.observable
                () => {
                    return {
                        [symbolObservable]: observable,
                        subscribe: createSubscribe<A>(subs),
                    };
                };

            return observable();
        }
    };
