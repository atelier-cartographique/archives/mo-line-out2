/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import * as debug from "debug";
import * as SockJS from "sockjs-client";
import { Client as StompClient, Message, Frame, over } from "stompjs";
import { from, Subscriber, Stream } from "most";
import { create as createObservable, Emitter } from "./Observable";

const log = debug("mo:receiver");

type ResolveFn = (c: StompClient) => void;
type RejectFn = (e: Error) => void;


const connect: (a: StompClient, b: (() => void)) => Promise<StompClient> =
    (client, errorCallback) => {
        const login = "", password = "";
        const resolver = (resolve: ResolveFn, _reject: RejectFn) => {
            log("connect.resolver");
            client.connect(
                login, password,
                (_fr: Frame) => resolve(client),
                errorCallback
            );
        };
        return (new Promise(resolver));
    };


const subscribe: (a: string) => (b: StompClient) => Stream<Message> =
    (dest) => (client) => {
        const emitter: Emitter<Message> =
            (subs) => {
                client.subscribe(dest, (msg: Message) => {
                    subs.forEach((sub) => {
                        sub.next(msg);
                    });
                });
            };

        const subs: Array<Subscriber<Message>> = [];
        const obs = createObservable<Message>(emitter);
        return from<Message>(obs(subs));
    };


const getStompClient: (a: string) => StompClient =
    (url) => over(<WebSocket>(new SockJS(url)));


const getClientStream: (a: string) => Stream<StompClient> =
    (url) => {

        const emitter: Emitter<StompClient> = () => {
            let interval = 1;
            const reConnect = () => {
                connect(getStompClient(url), () => {
                    if (interval < 30000) {
                        interval = interval * 2;
                    }
                    log(`Lost Connection To STOMP Server, retrying in ${interval / 1000} seconds`);
                    setTimeout(reConnect, interval);
                })
                    .then((client: StompClient) => {
                        interval = 1;
                        subs.forEach((sub) => {
                            sub.next(client);
                        });
                    });
            };
            reConnect();
        };

        const subs: Array<Subscriber<StompClient>> = [];
        const obs = createObservable<StompClient>(emitter);
        return from<StompClient>(obs(subs));
    };

export const createStream: (a: string, b: string) => Stream<Message> =
    (url, destination) => {
        const sub = subscribe(destination);
        return getClientStream(url).chain(sub);
    };
