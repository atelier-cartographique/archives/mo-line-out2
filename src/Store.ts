/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Stream, Subscriber, from } from "most";
import { Maybe, Some, None, List, IO } from "monet";
import { create as createObservable, Emitter } from "./Observable";

const logger = debug('mo:store');

interface StompParameters {
    visitId: number;
    ticket: string;
    serviceIntName?: string;
    servicePointName?: string;
};

export type EventName =
    "VISIT_CREATE" | "VISIT_CALL" | "VISIT_NEXT" | "VISIT_END";

export interface StompBody {
    eventName: EventName;
    eventTime: string;
    parameters: StompParameters;
}

export type State = List<StompBody>;
type UpdateFn = (a: State) => State;
type Updater = (a: UpdateFn) => State;

export interface Store {
    update: Updater;
    stream: Stream<State>;
}

export const store: Store = (() => {
    let state = List<StompBody>();

    const emitter: Emitter<State> =
        (subs) => {
            let previousState = List<StompBody>();
            setInterval(() => {
                if (previousState !== state) {
                    subs.forEach((sub) => {
                        sub.next(state);
                    });
                    previousState = state;
                }
            }, 100);
        };

    const subs: Array<Subscriber<State>> = [];
    const obs = createObservable<State>(emitter);

    const updateState: Updater =
        (fn) => {
            state = fn(state);
            state.map((sb) => {
                logger(`${sb.eventName} ${sb.parameters.ticket} ${sb.parameters.visitId}`);
            });
            return state;
        };
    return {
        update: updateState,
        stream: from<State>(obs(subs))
    };
})();



export const extractBody: (a: string) => Maybe<StompBody> =
    (body) => {
        try {
            const data: StompBody = JSON.parse(body);
            return Some<StompBody>(data);
        }
        catch (e) {
            return None<StompBody>();
        }
    };


const filterTicket: (a: number) => (b: StompBody) => boolean =
    (vid) => (r) => {
        return vid !== r.parameters.visitId;
    };


const filterTicketName: (a: string) => (b: StompBody) => boolean =
    (tid) => (r) => {
        return tid !== r.parameters.ticket;
    };


const visitCreate: (a: StompBody) => void =
    (_data) => {
        // store.update((state) => state.cons(data));
    };

const visitCall: (a: StompBody) => void =
    (data) => {
        // store.update((state) => state.cons(data));
        const filterId = filterTicket(data.parameters.visitId);
        const filterName = filterTicketName(data.parameters.ticket);
        store.update((state) =>
            state.filter(filterId).filter(filterName).cons(data));
    };

const visitNext: (a: StompBody) => void =
    (data) => {
        const filterId = filterTicket(data.parameters.visitId);
        const filterName = filterTicketName(data.parameters.ticket);
        store.update((state) =>
            state.filter(filterId).filter(filterName).cons(data));
    };

const visitEnd: (a: StompBody) => void =
    (data) => {
        const filterId = filterTicket(data.parameters.visitId);
        const filterName = filterTicketName(data.parameters.ticket);
        store.update((state) => state.filter(filterId).filter(filterName));
    };



export const update: (a: StompBody) => void =
    (data) => {
        const { eventName, parameters } = data;
        switch (eventName) {
            case "VISIT_CREATE":
                visitCreate(data);
                break;
            case "VISIT_CALL":
                visitCall(data);
                break;
            case "VISIT_NEXT":
                visitNext(data);
                break;
            case "VISIT_END":
                visitEnd(data);
                break;
            default:
                break;
        }
    };

logger('loaded');
