/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from "debug";
import { createConnection, Socket } from "net";
import { Stream, Subscriber, from, fromEvent, unfold } from "most";
import { Maybe, Some, None, List, IO } from "monet";
import { create as createObservable, Emitter } from "./Observable";
import { State, StompBody, EventName, store } from "./Store";
import {
    text,
    display,
    createMessage,
    formatMessage,
    MessageSequence,
    MessageData,
    MessageBuffer,
    clearSegments
} from "./Message";


const log = debug("mo:sink");

const FRAME_RATE = 2000;

const sortMessages: (a: State) => State =
    (state) => {
        const stateArray = state.toArray().sort((a, b) => {
            const tsa = Date.parse(a.eventTime);
            const tsb = Date.parse(b.eventTime);
            // return (tsa > tsb ? 1 : -1);
            return (tsa < tsb ? 1 : -1);
        });
        return List.fromArray<StompBody>(stateArray);
    };


const filterType: (a: EventName) => (b: StompBody) => boolean =
    (name) => (data) => {
        return data.eventName !== name;
    };

const withFloor: (a: string) => (b: StompBody) => boolean =
    (prefix) => (data) => {
        if (data.parameters.servicePointName) {
            const parts = data.parameters.servicePointName.split('_');
            if (parts.length > 1) {
                return parts[1].charAt(0) === prefix.charAt(0);
            }
        }
        return data.parameters.ticket.charAt(0) === prefix.charAt(0);
    };

const listTake: (a: number) => ((b: State, c: StompBody) => State) =
    (max) => {
        let idx = 0;
        return (acc, data) => {
            const cn = idx;
            idx = idx + 1;
            if (cn < max) {
                return acc.cons(data);
            }
            return acc;
        };
    };


type Writer = IO<Buffer>;


const format: (a: string) => (b: State) => State =
    (prefix) => (state) => {
        const results =
            sortMessages(state)
                .filter(filterType("VISIT_CREATE"))
                .filter(filterType("VISIT_END"))
                .filter(withFloor(prefix))
                .foldLeft(List<StompBody>())(listTake(3));
        //.reverse();

        return results;
    };


const unfolder: (a: string) => (b: State) => Stream<string> =
    (prefix) => (state) => {
        const results: string[] = [];
        if (state.size() > 0) {
            const folder: (a: string, b: StompBody) => string =
                (acc, data) => {
                    const ticket = data.parameters.ticket;
                    const tail = ticket.slice(1);
                    if (data.parameters.servicePointName) {
                        const parts = data.parameters.servicePointName.split('_');
                        if (parts.length > 1) {
                            const floor = parts[1].charAt(0);
                            return acc + floor + tail + "  ";
                        }
                    }
                    return acc + prefix + tail + "  ";
                };

            const f = state.foldLeft<string>(`${prefix}> `);
            results.push(f(folder));
        }
        return from(results);
    };


const write: (a: Writer) => void =
    (io) => {
        io.run();
    };


type PanelFn = (a: Maybe<Socket>) => void;

const connectPanel: (a: string, b: number, c: PanelFn) => (c: number) => void =
    (host, port, letPanel) => (interval) => {
        log(`connectPanel ${interval}`);
        const connectOptions = { host, port };
        const innerConnect = connectPanel(host, port, letPanel);
        let newInterval = interval * 2;

        if (newInterval > 60000) {
            newInterval = 60000;
        }

        const reconnect = (err?: Error) => {
            letPanel(None<Socket>());
            log(`
Lost Connection To Panel [${host}:${port}].
${err}
Retrying in ${newInterval / 1000} seconds.`);

            setTimeout(() => {
                innerConnect(newInterval);
            }, newInterval);
        };

        const sock = createConnection(connectOptions, () => {
            log(`connected to panel ${host}:${port}`);
            letPanel(Some<Socket>(sock));
            newInterval = 1;
        });

        sock.on("end", reconnect);
        sock.on("error", reconnect);
    }




const panelBuffer = (() => {
    let buffers = List<Buffer>();
    let sock: Socket;
    let answers: Stream<Buffer>;
    let intId: number;


    const push: (a: Buffer) => void =
        (buf) => {
            buffers = buffers.cons(buf);
        };

    const run: () => void =
        () => {
            if (sock) {
                const buf = buffers.headMaybe();
                buffers = buffers.tail();
                if (buf.isSome()) {
                    try {
                        sock.write(buf.some());
                    }
                    catch (err) {
                        log(err);
                    }
                }
            }
        };

    setInterval(run, FRAME_RATE);


    const attach: (a: Socket) => void =
        (s) => {
            sock = s;
            const emitter: Emitter<Buffer> =
                (subs) => {
                    sock.on("data", (frame) => {
                        subs.forEach((sub) => {
                            log(`frame: ${frame}`);
                            sub.next(frame);
                        });
                    });
                };

            const subs: Array<Subscriber<Buffer>> = [];
            const obs = createObservable<Buffer>(emitter);
            answers = from<Buffer>(obs(subs));
        };

    return { attach, push };
})();


export const create: (b: string, c: number) => void =
    (host, port) => {
        let panel = None<Socket>();

        const connect = connectPanel(host, port, (mp) => {
            panel = mp;
            if (panel.isSome()) {
                panelBuffer.attach(panel.some());
                panelBuffer.push(clearSegments(0, 0));
                panelBuffer.push(clearSegments(0, 1));
            }
        });

        const writer: (a: string) => (b: string) => Writer =
            (prefix) => (data) => {
                const prefixString: string = (prefix === "Y") ? "0" : "1";
                const prefixDest: number = (prefix === "Y") ? 0 : 1;
                const f: () => Buffer =
                    () => {
                        const md: MessageData = [];
                        md.push(text(0, 0, data));
                        md.push(display(0, 0));
                        const msg = createMessage(md);
                        const buf = formatMessage(0, prefixDest, msg);

                        if (panel.isSome()) {
                            log(`write ${buf}`);
                            // panel.some().write(buf);
                            panelBuffer.push(buf);
                        }

                        return buf;
                    };
                return IO<Buffer>(f);
            };


        connect(1);

        store.stream
            .map<State>(format("0"))
            .chain<string>(unfolder("0"))
            .map<Writer>(writer("Y"))
            .observe(write)
            .catch(err => log(err));


        store.stream
            .map<State>(format("1"))
            .chain<string>(unfolder("1"))
            .map<Writer>(writer("Z"))
            .observe(write)
            .catch(err => log(err));

    }
