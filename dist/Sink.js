"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const debug = require("debug");
const net_1 = require("net");
const most_1 = require("most");
const monet_1 = require("monet");
const Observable_1 = require("./Observable");
const Store_1 = require("./Store");
const Message_1 = require("./Message");
const log = debug("mo:sink");
const FRAME_RATE = 2000;
const sortMessages = (state) => {
    const stateArray = state.toArray().sort((a, b) => {
        const tsa = Date.parse(a.eventTime);
        const tsb = Date.parse(b.eventTime);
        // return (tsa > tsb ? 1 : -1);
        return (tsa < tsb ? 1 : -1);
    });
    return monet_1.List.fromArray(stateArray);
};
const filterType = (name) => (data) => {
    return data.eventName !== name;
};
const withFloor = (prefix) => (data) => {
    if (data.parameters.servicePointName) {
        const parts = data.parameters.servicePointName.split('_');
        if (parts.length > 1) {
            return parts[1].charAt(0) === prefix.charAt(0);
        }
    }
    return data.parameters.ticket.charAt(0) === prefix.charAt(0);
};
const listTake = (max) => {
    let idx = 0;
    return (acc, data) => {
        const cn = idx;
        idx = idx + 1;
        if (cn < max) {
            return acc.cons(data);
        }
        return acc;
    };
};
const format = (prefix) => (state) => {
    const results = sortMessages(state)
        .filter(filterType("VISIT_CREATE"))
        .filter(filterType("VISIT_END"))
        .filter(withFloor(prefix))
        .foldLeft(monet_1.List())(listTake(3));
    //.reverse();
    return results;
};
const unfolder = (prefix) => (state) => {
    const results = [];
    if (state.size() > 0) {
        const folder = (acc, data) => {
            const ticket = data.parameters.ticket;
            const tail = ticket.slice(1);
            if (data.parameters.servicePointName) {
                const parts = data.parameters.servicePointName.split('_');
                if (parts.length > 1) {
                    const floor = parts[1].charAt(0);
                    return acc + floor + tail + "  ";
                }
            }
            return acc + prefix + tail + "  ";
        };
        const f = state.foldLeft(`${prefix}> `);
        results.push(f(folder));
    }
    return most_1.from(results);
};
const write = (io) => {
    io.run();
};
const connectPanel = (host, port, letPanel) => (interval) => {
    log(`connectPanel ${interval}`);
    const connectOptions = { host, port };
    const innerConnect = connectPanel(host, port, letPanel);
    let newInterval = interval * 2;
    if (newInterval > 60000) {
        newInterval = 60000;
    }
    const reconnect = (err) => {
        letPanel(monet_1.None());
        log(`
Lost Connection To Panel [${host}:${port}].
${err}
Retrying in ${newInterval / 1000} seconds.`);
        setTimeout(() => {
            innerConnect(newInterval);
        }, newInterval);
    };
    const sock = net_1.createConnection(connectOptions, () => {
        log(`connected to panel ${host}:${port}`);
        letPanel(monet_1.Some(sock));
        newInterval = 1;
    });
    sock.on("end", reconnect);
    sock.on("error", reconnect);
};
const panelBuffer = (() => {
    let buffers = monet_1.List();
    let sock;
    let answers;
    let intId;
    const push = (buf) => {
        buffers = buffers.cons(buf);
    };
    const run = () => {
        if (sock) {
            const buf = buffers.headMaybe();
            buffers = buffers.tail();
            if (buf.isSome()) {
                try {
                    sock.write(buf.some());
                }
                catch (err) {
                    log(err);
                }
            }
        }
    };
    setInterval(run, FRAME_RATE);
    const attach = (s) => {
        sock = s;
        const emitter = (subs) => {
            sock.on("data", (frame) => {
                subs.forEach((sub) => {
                    log(`frame: ${frame}`);
                    sub.next(frame);
                });
            });
        };
        const subs = [];
        const obs = Observable_1.create(emitter);
        answers = most_1.from(obs(subs));
    };
    return { attach, push };
})();
exports.create = (host, port) => {
    let panel = monet_1.None();
    const connect = connectPanel(host, port, (mp) => {
        panel = mp;
        if (panel.isSome()) {
            panelBuffer.attach(panel.some());
            panelBuffer.push(Message_1.clearSegments(0, 0));
            panelBuffer.push(Message_1.clearSegments(0, 1));
        }
    });
    const writer = (prefix) => (data) => {
        const prefixString = (prefix === "Y") ? "0" : "1";
        const prefixDest = (prefix === "Y") ? 0 : 1;
        const f = () => {
            const md = [];
            md.push(Message_1.text(0, 0, data));
            md.push(Message_1.display(0, 0));
            const msg = Message_1.createMessage(md);
            const buf = Message_1.formatMessage(0, prefixDest, msg);
            if (panel.isSome()) {
                log(`write ${buf}`);
                // panel.some().write(buf);
                panelBuffer.push(buf);
            }
            return buf;
        };
        return monet_1.IO(f);
    };
    connect(1);
    Store_1.store.stream
        .map(format("0"))
        .chain(unfolder("0"))
        .map(writer("Y"))
        .observe(write)
        .catch(err => log(err));
    Store_1.store.stream
        .map(format("1"))
        .chain(unfolder("1"))
        .map(writer("Z"))
        .observe(write)
        .catch(err => log(err));
};
