"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const Receiver_1 = require("./Receiver");
const Store_1 = require("./Store");
const Sink_1 = require("./Sink");
const consume = (data) => {
    if (data.isSome()) {
        Store_1.update(data.some());
    }
};
const toBody = (msg) => Store_1.extractBody(msg.body);
const plug = (source, destination) => {
    const rePlug = () => {
        console.error("Stream Interrupted, Replug");
        setTimeout(() => plug(source, destination), 1000);
    };
    Receiver_1.createStream(source, destination)
        .map(toBody)
        .observe(consume)
        .then(rePlug)
        .catch(rePlug);
};
const main = (source, destination, panelHost, panelPort) => {
    Sink_1.create(panelHost, parseInt(panelPort));
    plug(source, destination);
};
exports.default = main;
