"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const debug = require("debug");
const most_1 = require("most");
const monet_1 = require("monet");
const Observable_1 = require("./Observable");
const logger = debug('mo:store');
;
exports.store = (() => {
    let state = monet_1.List();
    const emitter = (subs) => {
        let previousState = monet_1.List();
        setInterval(() => {
            if (previousState !== state) {
                subs.forEach((sub) => {
                    sub.next(state);
                });
                previousState = state;
            }
        }, 100);
    };
    const subs = [];
    const obs = Observable_1.create(emitter);
    const updateState = (fn) => {
        state = fn(state);
        state.map((sb) => {
            logger(`${sb.eventName} ${sb.parameters.ticket} ${sb.parameters.visitId}`);
        });
        return state;
    };
    return {
        update: updateState,
        stream: most_1.from(obs(subs))
    };
})();
exports.extractBody = (body) => {
    try {
        const data = JSON.parse(body);
        return monet_1.Some(data);
    }
    catch (e) {
        return monet_1.None();
    }
};
const filterTicket = (vid) => (r) => {
    return vid !== r.parameters.visitId;
};
const filterTicketName = (tid) => (r) => {
    return tid !== r.parameters.ticket;
};
const visitCreate = (_data) => {
    // store.update((state) => state.cons(data));
};
const visitCall = (data) => {
    // store.update((state) => state.cons(data));
    const filterId = filterTicket(data.parameters.visitId);
    const filterName = filterTicketName(data.parameters.ticket);
    exports.store.update((state) => state.filter(filterId).filter(filterName).cons(data));
};
const visitNext = (data) => {
    const filterId = filterTicket(data.parameters.visitId);
    const filterName = filterTicketName(data.parameters.ticket);
    exports.store.update((state) => state.filter(filterId).filter(filterName).cons(data));
};
const visitEnd = (data) => {
    const filterId = filterTicket(data.parameters.visitId);
    const filterName = filterTicketName(data.parameters.ticket);
    exports.store.update((state) => state.filter(filterId).filter(filterName));
};
exports.update = (data) => {
    const { eventName, parameters } = data;
    switch (eventName) {
        case "VISIT_CREATE":
            visitCreate(data);
            break;
        case "VISIT_CALL":
            visitCall(data);
            break;
        case "VISIT_NEXT":
            visitNext(data);
            break;
        case "VISIT_END":
            visitEnd(data);
            break;
        default:
            break;
    }
};
logger('loaded');
