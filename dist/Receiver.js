"use strict";
/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const debug = require("debug");
const SockJS = require("sockjs-client");
const stompjs_1 = require("stompjs");
const most_1 = require("most");
const Observable_1 = require("./Observable");
const log = debug("mo:receiver");
const connect = (client, errorCallback) => {
    const login = "", password = "";
    const resolver = (resolve, _reject) => {
        log("connect.resolver");
        client.connect(login, password, (_fr) => resolve(client), errorCallback);
    };
    return (new Promise(resolver));
};
const subscribe = (dest) => (client) => {
    const emitter = (subs) => {
        client.subscribe(dest, (msg) => {
            subs.forEach((sub) => {
                sub.next(msg);
            });
        });
    };
    const subs = [];
    const obs = Observable_1.create(emitter);
    return most_1.from(obs(subs));
};
const getStompClient = (url) => stompjs_1.over((new SockJS(url)));
const getClientStream = (url) => {
    const emitter = () => {
        let interval = 1;
        const reConnect = () => {
            connect(getStompClient(url), () => {
                if (interval < 30000) {
                    interval = interval * 2;
                }
                log(`Lost Connection To STOMP Server, retrying in ${interval / 1000} seconds`);
                setTimeout(reConnect, interval);
            })
                .then((client) => {
                interval = 1;
                subs.forEach((sub) => {
                    sub.next(client);
                });
            });
        };
        reConnect();
    };
    const subs = [];
    const obs = Observable_1.create(emitter);
    return most_1.from(obs(subs));
};
exports.createStream = (url, destination) => {
    const sub = subscribe(destination);
    return getClientStream(url).chain(sub);
};
